package com.fourcatsdev.aula08.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.aula08.modelo.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
